<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$activitate = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";
$treasure="";
$talent="";
$facultate="";
$sex="";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['activitate'])){
	$activitate = $_POST['activitate'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
if(isset($_POST['treasure'])){
	$treasure = $_POST['treasure'];
}
if(isset($_POST['talent'])){
	$talent = $_POST['talent'];
}
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}
//Erori de inserare
//Optiune
if ($talent=="da"&&$treasure=="da") {
	$activitate="Ambele";
}
else if ($talent=="da") {
	$activitate="Proba de Talent";
}else if ($treasure=="da") {
	$activitate="Treasure Hunt";
}else
{
$error = 1;
	$error_text = "Introduceti cel putin o activitate";
}
//campuri libere
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth)||empty($activitate)||empty($facebook)||empty($cnp))
{
	$error = 1;
	$error_text = "One or more fields are empty!";
}else
{
	//Calculare sex

if ($cnp[0]==1
||$cnp[0]==3
||$cnp[0]==5) {
	$sex="M";
}else $sex="F";

	//validare cnp si data de nastere
//zi
$a=$cnp[5]*10+$cnp[6];
$b=$birth[8]*10+$birth[9];
if ($a!=$b) {
	$error = 1;
	$error_text = "CNP si data de nastere sunt incompatibile";
}
//luna
$a=$cnp[3]*10+$cnp[4];
$b=$birth[5]*10+$birth[6];
if ($a!=$b) {
	$error = 1;
	$error_text = "CNP si data de nastere sunt incompatibile";
}
//an
$a=$cnp[1]*10+$cnp[2];
$b=$birth[2]*10+$birth[3];
if ($a!=$b) {
	$error = 1;
	$error_text = "CNP si data de nastere sunt incompatibile";
}
//
}
//
//erori de lungime
if(strlen($firstname) < 3 || strlen($lastname) < 3)
{
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}
if (strlen($firstname) > 20|| strlen($lastname) > 20) {
	$error = 1;
	$error_text = "First or Last name is longer than expected!";
}
if (strlen($question)<15) {
	$error = 1;
    $error_text = "Question is shorter than expected!";
}
if(!is_numeric($phone) || strlen($phone)!=10)
{
	$error = 1;
	$error_text = "Phone number is not valid";
}
if (is_numeric($firstname)||is_numeric($lastname) ) {
	$error = 1;
	$error_text = "First or Last name is not valid";
}
//validare email
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$error = 1;
	$error_text = "Email is not valid";
}
//validare cnp
if (strlen($cnp)<13
	||$cnp[0]=="0"
    ||$cnp[0]=="7" 
	||$cnp[0]=="8"
    ||$cnp[0]=="9")
{
	$error = 1;
	$error_text = "CNP is not valid";
}
//Validare Facebook
if (!(strpos($facebook, 'facebook')!==false)) {
	$error = 1;
	$error_text = "Linkul de facebook este incorect";
}
//Validare Varsta
$today=date("y-m-d");
$diff = date_diff(date_create($birth), date_create($today));
if ($diff->format('%y')<18) {
$error = 1;
	$error_text = "Data nasterii nu este valabila";}
//Validare Captcha
if ($captcha_generated!=$captcha_inserted) {
$error = 1;
	$error_text = "Captcha code is not valid";
}
//validare termeni si conditii
if (empty($check)) {
	$error = 1;
	$error_text = "Termenii si conditiile trebuiesc acceptate";
}


try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
if($error == 0) {
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,activitate,question,facultate,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:activitate,:question,:facultate,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':activitate',$activitate);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':sex',$sex);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Cererea a fost inregistrata";
}
}else{
    echo $error_text;
}

