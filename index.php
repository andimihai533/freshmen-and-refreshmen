<!DOCTYPE html>
<html>
<head>
	<title>Freshmen & Refresmen</title>
	<meta charset="UTF-8">
	<script src="assets/jquery.js"></script>
	<link rel="stylesheet" href="assets/bootstrap.css">
	<link rel="stylesheet" href="assets/style.css">
</head>
<body>
<div id="container">
<div class="jumbotron">
<center>
<h4 style="margin-bottom:30px;">Formular de Înscriere</h4>

<form action="submit.php" method="post" name="first_form" id="first_form">
	<input class="inp" type="text" name="firstname" placeholder="Prenume">
	<input class="inp" type="text" name="lastname" placeholder="Nume">
	<input class="inp" type="text" name="phone" placeholder="Numar de telefon">
	<input class="inp" type="text" name="email" placeholder="Email">
	<input class="inp" type="text" name="facebook" placeholder="Facebook">

<center><label for="facultate">Alege specializarea</label></center>

<center> 
  <select name="facultate">
    <option value="cibernetica">Cibernetica Economica</option>
    <option value="statistica">Statistica</option>
    <option value="informatica">Informatica Economica</option>
  </select>
  <br><br>
   <br><br>
</center>
	<center><label for="birth">Zi de nastere</label></center>
	<input class="inp" type="date" name="birth" placeholder="Zi de nastere">
	  <br><br>
	<center><label for="activitate">Activitate</label></center>
		<center><input type="checkbox" name="talent" id="talent" default value="da">
		<label for="talent">Cyberkids got Talent</label></center>
		<center><input type="checkbox" name="treasure" id="treasure" value="da">
		<label for="treasure">Break Through Cybertron</label></center>
		  <br><br>
	<label for="question">De ce vrei sa participi?</label>
	 <br><br>
	<textarea class="inp" name="question"></textarea>
	 <br><br>
	<label for="captcha_generated">Completeaza urmatorul cod captcha</label>
	<?php
	$digits = 5;
	$captcha = rand(pow(10, $digits-1), pow(10, $digits)-1);
	echo '<input class="inp" type="text" name="captcha_generated" readonly="readonly" style="background-color:#d3d3d3;" value="';
	echo $captcha;
	echo '">';
	?>
	<input class="inp" type="text" name="captcha_inserted" placeholder="Scrie aici numarul">
	
	<div class="row">
      <h4>Termeni si Conditii</h4>
      <div class="input-group">
        <input type="checkbox" id="check" name="check"/>
        <label for="check">Confirm ca am citit si sunt de acord cu termenii si conditiile</label>
      </div>
    </div>
	<input class="inp" type="submit">
</form>
</center>
</div>
</div>
</body>
</html>

<script type="text/javascript">
//Callback handler for form submit event
$("#first_form").submit(function(e)
{
 
    var formObj = $(this);
    var formURL = formObj.attr("action");
    var formData = new FormData(this);
    $.ajax({
        url: formURL,
    type: 'POST',
        data:  formData,
    mimeType:"multipart/form-data",
    contentType: false,
        cache: false,
        processData:false,
    success: function(data, textStatus, jqXHR)
    {
         console.log(data);
    },
     error: function(jqXHR, textStatus, errorThrown) 
     {
     }          
    });
    e.preventDefault(); //Prevent Default action. 
}); 
$("#multiform").submit(); //Submit the form
</script>